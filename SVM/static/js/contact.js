document.addEventListener('DOMContentLoaded', function() {
    var openContactBtn = document.getElementById('open-contact-form');
    var closeContactBtn = document.querySelector('.close-btn');
    var contactFormContainer = document.querySelector('.contact-form-container');

    openContactBtn.addEventListener('click', function() {
        contactFormContainer.style.display = 'block';
    });

    closeContactBtn.addEventListener('click', function() {
        contactFormContainer.style.display = 'none';
    });

    contactFormContainer.addEventListener('click', function(event) {
        if (event.target === contactFormContainer) {
            contactFormContainer.style.display = 'none';
        }
    });

    var contactForm = document.getElementById('contact-form');
    contactForm.addEventListener('submit', function(event) {
        event.preventDefault();
        var name = document.getElementById('name').value;
        var email = document.getElementById('email').value;
        var subject = document.getElementById('subject').value;
        
        var mailtoLink = `mailto:12220087.gcit@rub.edu.bt?subject=${encodeURIComponent(subject)}&body=${encodeURIComponent('Name: ' + name + '\nEmail: ' + email + '\nSubject: ' + subject)}`;
        window.location.href = mailtoLink;
    });
});